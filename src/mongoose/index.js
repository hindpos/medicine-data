import Promise from 'bluebird';

const mongoose = require('mongoose');

const mongoDbURI = 'mongodb://localhost:27017';
const database = 'posapp';

mongoose.Promise = Promise;

const mongooseConnect = () => mongoose.connect(`${mongoDbURI}/${database}`);

module.exports = { mongooseConnect };
