/* eslint no-bitwise: ["error", { "allow": ["~"] }] */
import 'babel-core/register';
import 'babel-polyfill';

import cheerio from 'cheerio';
// import rp from 'request-promise';
// ~~ is used to convert NaN to 0
import parseNum from 'parse-num';
import fs from 'fs';
import asyncLoop from 'node-async-loop';
import xpath from 'xpath';
import { MongoClient } from 'mongodb';
import { DOMParser as Dom } from 'xmldom';
import mongoose from 'mongoose';

import GlobalProduct from '../models/globalProduct';

const mongoUrl = 'mongodb://localhost:27017';
const dbName = 'posapp';

const getNum = (str, mul) => {
  const num = parseNum(str);
  if (Number.isNaN(num)) {
    return 0;
  }
  return Math.round(num * mul);
};

const otcParser = ($, $2, url) => {
  const otcs = [];
  const otcName = $('.ProductTitle__product-title___3QMYH').text();
  const company = $('.ProductTitle__manufacturer___sTfon').text();
  // const combosel = '.ComboPackItem__combo-name___3-uyz';
  const price = [];
  const units = [];
  const name = [];
  const discountedMrpPri = xpath.select(
    '//span[@class="DiscountDetails__discount-price___Mdcwo"]/text()',
    $2,
  ).toString();
  const originalMrpPri = xpath.select(
    '//div[@class="PriceDetails__mrp-tag___3WdTI"]/../text()',
    $2,
  ).toString();
  const discountedMrpSec = xpath.select(
    '//span[@class="ComboPackItem__discount-price___1twKy"]/text()',
    $2,
  ).map(e => e.nodeValue).filter(e => !Number.isNaN(parseFloat(e)));
  const discountedQuanSec = xpath.select(
    '//div[@class="ComboPackItem__combo-name___3-uyz"]/text()',
    $2,
  ).map(e => e.nodeValue);

  if (discountedMrpPri.length > 0) {
    price.push(getNum(discountedMrpPri, 100));
    units.push(1);
    name.push(otcName);
  } else if (originalMrpPri.length > 0) {
    price.push(getNum(originalMrpPri, 100));
    units.push(1);
    name.push(otcName);
  }
  for (let ind = 0; ind < discountedQuanSec.length; ind += 1) {
    price.push(getNum(discountedMrpSec[ind], 100));
    units.push(getNum(discountedQuanSec[ind], 1));
    name.push(`${otcName} ${discountedQuanSec[ind]}`);
  }
  for (let ind = 0; ind < units.length; ind += 1) {
    otcs.push({
      name: name[ind],
      details: {
        company,
        url,
      },
      price: price[ind],
      unitPrice: price[0],
      units: units[ind],
    });
  }
  return otcs;
};

const drugParser = ($, url) => {
  const name = $('.DrugInfo__drug-name-heading___adCs-').text();
  const company = $('.DrugInfo__company-name___39Abk').text();
  const compositionRaw = $('.DrugInfo__salt-name___2-9Vh').text();
  const composition = compositionRaw.split('+').map((e) => {
    const chemQuant = e.split('(');
    if (chemQuant.length > 1) {
      const chemical = chemQuant[0].trim();
      const quantity = chemQuant[1].split(')')[0].trim();
      return { chemical, quantity };
    }
    return 0;
  }).filter(e => e !== 0);
  const prescriptionRaw = $('.DrugInfo__prescription-requirement___21T_g').text();
  const prescription = (prescriptionRaw.split(' ').pop() === 'Required');
  const use = $('.DrugInfo__uses___381Re').text().split(',');
  const quantity = $('.DrugPriceBox__quantity___2LGBX').text();
  const price = getNum($('.DrugPriceBox__price___dj2lv').text(), 100);
  let unitPrice = getNum($('div[class^="DrugPriceBox__price-"]').text(), 100);
  if (unitPrice === 0) {
    unitPrice = price;
  }
  let units = 1;
  if (price > 0 && price > unitPrice) {
    units = getNum(quantity.split(' ')[0], 1);
  }
  return [{
    name,
    details: {
      company,
      composition,
      prescription,
      use,
      quantity,
      url,
    },
    price,
    unitPrice,
    units,
  }];
};


const path = process.argv[2];
if (!path) {
  console.log('Provide path to 1mg medicines folder');
}
const files = fs.readdirSync(path);

mongoose.connect('mongodb://localhost:27017/posapp', async () => {
  console.log('Connected correctly to server');
  await asyncLoop(files, async (file, next) => {
    await fs.readFile(`${path}/${file}`, 'utf8', async (err, data) => {
      const medUrl = Buffer.from(file, 'base64').toString('ascii');
      try {
        // console.log(`${path}/${file}`);
        const $ = cheerio.load(data);
        const $2 = new Dom().parseFromString(data);
        // console.log(`${medUrl}\n`);
        const urlParts = medUrl.split('/');
        const type = urlParts[urlParts.length - 2];
        let med = '';
        if (type === 'drugs') {
          med = drugParser($, medUrl);
        } else if (type === 'otc') {
          med = otcParser($, $2, medUrl);
        }
        // console.dir(med, { depth: 10 });
        /* eslint-disable no-await-in-loop */
        if (med.length > 0) {
          for (let index = 0; index < med.length; index += 1) {
            const prod = new GlobalProduct(med[index]);
            await prod.save();
          }
        }
      } catch (err2) {
        console.log(`\n### ${medUrl}`);
        console.log(err2);
      }
      next();
    });
  });
});
const run = async () => {
  let client;
  try {
    client = await MongoClient.connect(mongoUrl);
    console.log('Connected correctly to server');
    const db = client.db(dbName);
    await asyncLoop(files, async (file, next) => {
      await fs.readFile(`${path}/${file}`, 'utf8', async (err, data) => {
        try {
          // console.log(`${path}/${file}`);
          const $ = cheerio.load(data);
          const $2 = new Dom().parseFromString(data);
          const medUrl = Buffer.from(file, 'base64').toString('ascii');
          // console.log(`${medUrl}\n`);
          const urlParts = medUrl.split('/');
          const type = urlParts[urlParts.length - 2];
          let med = '';
          if (type === 'drugs') {
            med = drugParser($, medUrl);
          } else if (type === 'otc') {
            med = otcParser($, $2, medUrl);
          }
          // console.dir(med, { depth: 10 });
          if (med.length > 0) {
            await db.collection('rawProducts').insertMany(med);
            // console.log('\n');
          }
        } catch (err2) {
          console.log('Error occured');
        }
        next();
      });
    });
  } catch (err) {
    console.log(err.stack);
  }
  // client.close();
};

// run();
export default {
  run,
};
