import mongoose from 'mongoose';

const TaxSchema = mongoose.Schema({
  _id: false,
  type: {
    $type: String,
    enum: ['CGST', 'SGST', 'IGST', 'SAT', 'SET', 'ED', 'VAT', 'CD', 'ET'],
    required: true,
    // * CGST -> Central GST
    // * SGST -> State GST
    // * SAT -> Sales Tax
    // * SET -> Service Tax
    // * ED -> Excise Duty
    // * VAT -> Value Added Tax
    // * CD -> Customs Duty
    // * ET -> Entertainment Tax
  },
  percent: {
    $type: Number,
    set: v => Math.round(v),
    min: 0,
    max: 100,
  },
  amount: {
    $type: Number,
    set: v => Math.round(v),
    min: 0,
    validate: [Number.isSafeInteger, 'Number should be a safe integer'],
  },
}, { typeKey: '$type' });

export default {
  TaxSchema,
};
