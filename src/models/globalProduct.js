import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

import { TaxSchema } from './helperSchema';

const GlobalProductSchema = mongoose.Schema(
  {
    name: {
      $type: String,
      required: true,
      trim: true,
    },
    price: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    unitPrice: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    units: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    taxes: {
      $type: [TaxSchema],
    },
    tax: {
      $type: Number,
      set: v => Math.round(v),
      validate: [Number.isSafeInteger, 'Number should be a safe integer'],
    },
    details: {
      $type: mongoose.Schema.Types.Mixed, // A json coneverted to String
    },
  },
  { typeKey: '$type' },
);

GlobalProductSchema.plugin(timestamps);
const GlobalProduct = mongoose.model('GlobalProduct', GlobalProductSchema);

export default GlobalProduct;
