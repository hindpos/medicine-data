import cheerio from 'cheerio';
import rp from 'request-promise';
import parseNum from 'parse-num';

const url = 'https://www.1mg.com/drugs/amoxyclav-625-tablet-140781';
rp(url).then((htmlStr) => {
  const $ = cheerio.load(htmlStr);
  const med = {};
  med.name = $('.DrugInfo__drug-name-heading___adCs-').text();
  med.details = {
    company: $('.DrugInfo__company-name___39Abk').text(),
    composition: $('.DrugInfo__salt-name___2-9Vh').text().split('+').map((e) => {
      const chemQuant = e.split('(');
      const chemical = chemQuant[0].trim();
      const quantity = chemQuant[1].split(')')[0].trim();
      return { chemical, quantity };
    }),
    prescription: $('.DrugInfo__prescription-requirement___21T_g').text(),
    use: $('.DrugInfo__uses___381Re').text().split(','),
    quantity: $('.DrugPriceBox__quantity___2LGBX').text(),
  };
  med.price = parseNum($('.DrugPriceBox__price___dj2lv').text()) * 100;
  console.dir(med, { depth: 10 });
});
